@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3">

<div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
              @endif
              <a href="/cast/create" class="btn btn-primary mb-2">Buat Cast Baru</a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th style="width: 40px">Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $cast)
                    <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $cast -> nama }}</td>
                    <td>{{ $cast -> umur }}</td>
                    <td style="display: flex;">
                        <a href="/cast/{{$cast->id}}" class="btn btn-dark btn-sm">show</a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-success btn-sm">Edit</a>
                        <form action="/cast/{{$cast->id}}" method="post">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                
                    </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">Tidak Ada Data</td>
                    </tr>
                    
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
</div>

@endsection