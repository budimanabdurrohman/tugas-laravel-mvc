<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('register');
    }

    public function sapa(){
        return view('welcome');
    }

    public function sapa_post(Request $request){
    // dd($request->all());
    $nama_dpn = $request["nama_dpn"];
    $nama_blk = $request["nama_blk"];
        return view ('welcome',[
            'nama_dpn' => $nama_dpn,
            'nama_blk' => $nama_blk
        ]);
    }

}
