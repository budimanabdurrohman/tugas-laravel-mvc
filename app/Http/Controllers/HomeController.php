<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('home');
    }

    public function table(){
        return view('adminlte.items.table');
    }

    public function datatable(){
        return view('adminlte.items.datatable');
    }
}
